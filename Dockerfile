FROM node:10.10.0-alpine

COPY npm-shrinkwrap.json package.json /

RUN npm install -g

WORKDIR /code
CMD ["/usr/bin/find", ".", "-name", "*.js", "-exec", "/node_modules/jshint/bin/jshint", "{}", "+"]
